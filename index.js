//---------------------------------------------------------
// GAME LOGIC
//---------------------------------------------------------
var questionID = -1;
var questions = ["Ile kot ma nog?",
                 "Jak ma na imie krolowa Anglii?",
                 "Czy lubisz placki?"];

var answers =   [["cztery", "4"], ["mariola", "mariolka"], ["bardzo", "lubie", "tak"]];

var addQuestion = function(q, a) {
    for (var i = 0; i < questions.length; i++)
        if (questions[i] === q) return;

    questions.push(q);
    
    var aProcessed = a.toLowerCase().trim();

    answers.push([aProcessed]);
    addQuestionToDB(q, aProcessed);
};

var updateQuestions = function() { getQuestionsFromDB(); };

var nextQuestion = function() {
    questionID++;
    if (questionID >= questions.length) questionID = 0;
};

var checkAnswer = function(ans, username) {
    var anss = ans.toLowerCase().trim();
    var correct = answers[questionID];

    console.log('USER\'s answer: ' + anss    + ' :: ' + (typeof anss));
    console.log('RIGHT   answer: ' + correct + ' :: ' + (typeof correct));

    for (var i = 0; i < correct.length; i++)
        if (anss === correct[i]) {
            ranking[username] += 1;
            return true;
        }

    if (anss === correct) {
        ranking[username] += 1;
        return true;
    }

    return false;
};

var addUser = function(username) {
    console.log('registering user ' + username);
    users.push(username);
    ranking[username] = 0;
    publishRanking();
};


//---------------------------------------------------------
// LISTS AND ARRAYS
//---------------------------------------------------------
var users = [];
var conversation = [];
var timeouts = [];
var ranking = {};

//---------------------------------------------------------
// SERVER CODE
//---------------------------------------------------------
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var pg = require('pg');
var connectionStr = process.env.DATABASE_URL || 'postgres://localhost:5432/piotrek';

// database connection:
app.get('/questions', function (request, response) {
  pg.connect(connectionStr, function(err, client, done) {
    client.query('SELECT * FROM questions', function(err, result) {
      done();
      if (err)
       { console.error(err); response.send("Error " + err); }
      else
       { response.send(result.rows); }
    });
  });
});


var addQuestionToDB = function(question, answer) {
    pg.connect(connectionStr, function(err, client, done) {
        client.query('INSERT INTO questions (question, answer) VALUES (\'' + question + '\', \'' + answer + '\');',
                     function(err, result) { done();
                                             console.log('insert result: ' + result); });
    });
};



var getQuestionsFromDB = function() {
    pg.connect(connectionStr, function(err, client, done) {
        var query = client.query('SELECT * FROM questions'); 
        query.on('row', function(row) {
            console.log('row::::: ' + row);
            questions.push(row.question);
            answers.push(row.answer);
        });
    });
};


app.get('/', function(req, res) {
  console.log('Updating question bank...');
  updateQuestions();

  console.log('Handling GET request');
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket) {
  socket.on('chat message', function(nick, msg){
    console.log('sending a message: %s', msg);
    conversation.push([nick, msg]);
    
    var good = 'nope';
    if (checkAnswer(msg, nick)) {
        good = 'good';
        clearTimeouts();
    }
    io.emit('answer response', nick, msg.concat(':').concat(good)); 
    
    if (checkAnswer(msg)) {
        publishRanking();
        publishQuestion();
    }
  });

  socket.on('user join', function(username) {
    console.log('users: ' + users);
    console.log('conversation: ' + conversation);

    if (username === 'secret clear all') {
      users = [];
      ranking = {};
      conversation = [];
      questionID = 0;

      console.log('clearing all');
      return;
    } else {
      for (var i = 0; i < users.length; i++)
        if (username === users[i]) {
          io.emit('server error', 'Nickname already in use');
          return;
        }
    }

    addUser(username);

    io.emit('conversation replay', username, conversation);

    if (users.length == 1) publishQuestion();
    //publishQuestion();
  });

  socket.on('add question', function(q, anss) {
    addQuestion(q, anss);
  });

  socket.on('leave game', function(username) {
    for (var i = 0; i < users.length; i++)
      if (users[i] === username) users.splice(i, 1);

    if (users.length == 0) conversation = [];
  });
});

var serv = http.listen(process.env.PORT || 30505, function() {
  var host = serv.address().address;
  var port = serv.address().port;

  console.log('Chat server listening on http://%s:%s', host, port);
  console.log('Questions: ' + questions);
});


var timeoutNotify = function(timeLeft) {
    console.log('Sending server message...');
    var msg = '';
    if (timeLeft <= 0) msg = 'Time is up! Correct answer: ' + answers[questionID];
    else msg = 'Time left: ' + timeLeft + 's';
    
    io.emit('timeout', msg);

    if (timeLeft <= 0) publishQuestion();
};


var publishQuestion = function() {
    nextQuestion();
    var question = questions[questionID];
    conversation.push(['QUESTION', question]);
    io.emit('chat message', 'QUESTION', question);
    setTimeouts();
};

var publishRanking = function() {
    io.emit('ranking', ranking);
};

var setTimeouts = function() {
    timeouts.push(setTimeout(function() { timeoutNotify(15); }, 15000));
    timeouts.push(setTimeout(function() { timeoutNotify(10); }, 20000));
    timeouts.push(setTimeout(function() { timeoutNotify( 5); }, 25000));
    timeouts.push(setTimeout(function() { timeoutNotify( 4); }, 26000));
    timeouts.push(setTimeout(function() { timeoutNotify( 3); }, 27000));
    timeouts.push(setTimeout(function() { timeoutNotify( 2); }, 28000));
    timeouts.push(setTimeout(function() { timeoutNotify( 1); }, 29000));
    timeouts.push(setTimeout(function() { timeoutNotify( 0); }, 30000));
};

var clearTimeouts = function() {
    for (var i = 0; i < timeouts.length; i++)
        clearTimeout(timeouts[i]);
};

